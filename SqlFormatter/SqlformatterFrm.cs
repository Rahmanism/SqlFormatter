﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlFormatter
{
    public partial class sqlFormatterFrm : Form
    {
        public sqlFormatterFrm()
        {
            InitializeComponent();
        }

        private void GoBtnClick(object sender, EventArgs e)
        {
            // Get the formatted sql code
            resultTxt.Text = SqlFormatter.FormatTSql( sqlSourceTxt.Text );
        }

        private void CopyToClipboardBtnClick(object sender, EventArgs e)
        {
            Clipboard.SetText( resultTxt.Text );
        }

        private void AboutBtnClick(object sender, EventArgs e)
        {
            var aboutFrm = new AboutFrm();
            aboutFrm.ShowDialog();
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if ( e.Control && e.KeyCode == Keys.A ) {
                TextBox txt = (TextBox)sender;
                txt.SelectAll();
            }
        }

        private void SqlFormatterFrmKeyDown(object sender, KeyEventArgs e)
        {
            switch ( e.KeyCode ) {
                case Keys.F1:
                    AboutBtnClick( null, null );
                    break;
                case Keys.F2:
                    CopyToClipboardBtnClick( null, null );
                    break;
                case Keys.F5:
                    GoBtnClick( null, null );
                    break;
            }
        }
    }
}
