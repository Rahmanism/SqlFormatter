﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SqlFormatter
{
    /// <summary>
    /// Formats the given Sql code
    /// </summary>
    public static class SqlFormatter
    {
        public static string FormatTSql(string tSql)
        {
            IList<ParseError> errors;
            TSqlScript sqlFragment;
            using ( var reader = new StringReader( tSql ) ) {
                var parser = new TSql120Parser( initialQuotedIdentifiers: true );
                sqlFragment = (TSqlScript)parser.Parse( reader, out errors );
            }

            if ( errors != null && errors.Any() ) {
                var sb = new StringBuilder();
                foreach ( var error in errors )
                    sb.AppendLine( error.Message );

                throw new InvalidOperationException( sb.ToString() );
            }

            var sql110ScriptGenerator = new Sql120ScriptGenerator( new SqlScriptGeneratorOptions {
                SqlVersion = SqlVersion.Sql120
            } );
            string finalScript;
            sql110ScriptGenerator.GenerateScript( sqlFragment, out  finalScript );
            return finalScript;
        }
    }
}
