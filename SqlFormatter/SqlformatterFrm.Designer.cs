﻿namespace SqlFormatter
{
    partial class sqlFormatterFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) ) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolsPnl = new System.Windows.Forms.Panel();
            this.aboutBtn = new System.Windows.Forms.Button();
            this.copyToClipboardBtn = new System.Windows.Forms.Button();
            this.goBtn = new System.Windows.Forms.Button();
            this.sqlSourceTxt = new System.Windows.Forms.TextBox();
            this.resultTxt = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolsPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolsPnl
            // 
            this.toolsPnl.Controls.Add(this.aboutBtn);
            this.toolsPnl.Controls.Add(this.copyToClipboardBtn);
            this.toolsPnl.Controls.Add(this.goBtn);
            this.toolsPnl.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolsPnl.Location = new System.Drawing.Point(0, 0);
            this.toolsPnl.Name = "toolsPnl";
            this.toolsPnl.Size = new System.Drawing.Size(683, 37);
            this.toolsPnl.TabIndex = 0;
            // 
            // aboutBtn
            // 
            this.aboutBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutBtn.Location = new System.Drawing.Point(645, 8);
            this.aboutBtn.Name = "aboutBtn";
            this.aboutBtn.Size = new System.Drawing.Size(26, 23);
            this.aboutBtn.TabIndex = 2;
            this.aboutBtn.Text = "?";
            this.aboutBtn.UseVisualStyleBackColor = true;
            this.aboutBtn.Click += new System.EventHandler(this.AboutBtnClick);
            // 
            // copyToClipboardBtn
            // 
            this.copyToClipboardBtn.Location = new System.Drawing.Point(94, 8);
            this.copyToClipboardBtn.Name = "copyToClipboardBtn";
            this.copyToClipboardBtn.Size = new System.Drawing.Size(150, 23);
            this.copyToClipboardBtn.TabIndex = 1;
            this.copyToClipboardBtn.Text = "&Copy to clipboard (F2)";
            this.copyToClipboardBtn.UseVisualStyleBackColor = true;
            this.copyToClipboardBtn.Click += new System.EventHandler(this.CopyToClipboardBtnClick);
            // 
            // goBtn
            // 
            this.goBtn.Location = new System.Drawing.Point(12, 8);
            this.goBtn.Name = "goBtn";
            this.goBtn.Size = new System.Drawing.Size(75, 23);
            this.goBtn.TabIndex = 0;
            this.goBtn.Text = "&Go (F5)";
            this.goBtn.UseVisualStyleBackColor = true;
            this.goBtn.Click += new System.EventHandler(this.GoBtnClick);
            // 
            // sqlSourceTxt
            // 
            this.sqlSourceTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sqlSourceTxt.Location = new System.Drawing.Point(0, 0);
            this.sqlSourceTxt.Multiline = true;
            this.sqlSourceTxt.Name = "sqlSourceTxt";
            this.sqlSourceTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sqlSourceTxt.Size = new System.Drawing.Size(336, 338);
            this.sqlSourceTxt.TabIndex = 1;
            this.sqlSourceTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
            // 
            // resultTxt
            // 
            this.resultTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultTxt.Location = new System.Drawing.Point(0, 0);
            this.resultTxt.Multiline = true;
            this.resultTxt.Name = "resultTxt";
            this.resultTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultTxt.Size = new System.Drawing.Size(343, 338);
            this.resultTxt.TabIndex = 2;
            this.resultTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxKeyDown);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 37);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.sqlSourceTxt);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.resultTxt);
            this.splitContainer1.Size = new System.Drawing.Size(683, 338);
            this.splitContainer1.SplitterDistance = 336;
            this.splitContainer1.TabIndex = 3;
            // 
            // sqlFormatterFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 375);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolsPnl);
            this.KeyPreview = true;
            this.Name = "sqlFormatterFrm";
            this.Text = "SQL Formatter";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SqlFormatterFrmKeyDown);
            this.toolsPnl.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel toolsPnl;
        private System.Windows.Forms.TextBox sqlSourceTxt;
        private System.Windows.Forms.TextBox resultTxt;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button goBtn;
        private System.Windows.Forms.Button copyToClipboardBtn;
        private System.Windows.Forms.Button aboutBtn;
    }
}

